﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace CatClone
{
    public struct Points
    {
        double x;
        double y;
        public Points(double a, double b)
        {
            x = a;
            y = b;
        }
        public double X
        {
            get { return x; }
            set { x = value; }
        }
        public double Y
        {
            get { return y; }
            set { y = value; }
        }
        //Сюда впиши то, что нужно будет добавить в структуру точек

    }
    class ClassHelp
    {
    }
}
