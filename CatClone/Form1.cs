﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Numerics;

namespace CatClone
{
    public partial class Form1 : Form
    {
        Bitmap bmp;
        Graphics graph;
        SolidBrush BrushWhite = new SolidBrush(Color.White);
        SolidBrush BrushLightGray = new SolidBrush(Color.LightGray);
        SolidBrush BrushGray = new SolidBrush(Color.Gray);
        SolidBrush BrushDarkGray = new SolidBrush(Color.DarkGray);

        bool Shum;
        bool Imag;
        Pen ArgoPen = new Pen(Color.Purple, 1);
        Points center;

        Int32 Clear, X_Box, Y_Box;
        Int32 Shum_lvl;

        List<List<double>> Cupola;

        List<Complex> Spec_1D;
        List<List<Complex>> Spec_2D;
        // List<double> InCupol;
        public Form1()
        {
            InitializeComponent();
            //pictureBox1.Width = 100;
            //pictureBox1.Height = 100;
        }
        void Init()       // Функция инцилизации компонентов
        {
            X_Box = Convert.ToInt32(textBox3.Text);
            Y_Box = Convert.ToInt32(textBox4.Text);

            pictureBox1.Width = X_Box;
            pictureBox1.Height = Y_Box;
            Size size = pictureBox1.Size;
            center = new Points(pictureBox1.Width / 2, pictureBox1.Height / 2);

            bmp = new Bitmap(size.Width, size.Height);
            graph = Graphics.FromImage(bmp);

            Shum = Convert.ToBoolean(checkBox1.Checked);
            Imag = Convert.ToBoolean(checkBox2.Checked);
            Clear = Convert.ToInt32(textBox1.Text);
            Shum_lvl = Convert.ToInt32(textBox2.Text);

        }
        private void button1_Click(object sender, EventArgs e)
        {
            Init();
            Cupola = new List<List<double>>();

            if (Imag)
            {
                OpenFileDialog openFile = new OpenFileDialog();// создаем диалоговое окно
                openFile.ShowDialog();// открываем окно
                string FileName = openFile.FileName;// берем полный адрес картинки            
                pictureBox1.ImageLocation = FileName;// грузим картинку в pictureBox


                Bitmap bmp = new Bitmap(pictureBox1.ImageLocation);
                Color clold = bmp.GetPixel(0, 0);
                Color clnew = Color.FromArgb(clold.A, clold.R, 0, clold.B);
                for (int y = 0; y < bmp.Height; y++)
                    for (int x = 0; x < bmp.Width; x++)
                        if (bmp.GetPixel(x, y) == clold) bmp.SetPixel(x, y, clnew);
            }
            else { Mass_Funk(ref Cupola); } //заполняем массив для Гаусса

            graph.Clear(Color.Black);
            //graph.DrawImage
            // graph.Equals
           
            //InCupol = new List<double>();

            //DrawB_to_w(Cupola, graph);

            if (Shum)
            {
                ShumPod(ref Cupola);
            }

            DrawB_to_w(Cupola, graph);
        }

       
        

        private void button2_Click(object sender, EventArgs e)
        {

            bool Pow_Cheak = true;
            int Pow_Ind = 0;

            Chek_For2(pictureBox1.Width, ref Pow_Cheak, ref Pow_Ind);

            if (Pow_Cheak)
            {
                double New_dim = Math.Pow(2, Pow_Ind);

                for (int i = 0; i < pictureBox1.Height; i++)
                {

                    for (int j = pictureBox1.Width; j < New_dim; j++)
                    {

                        Cupola[i].Add(0);
                    }

                }
                
            }

            Chek_For2(pictureBox1.Height, ref Pow_Cheak, ref Pow_Ind);

            if (Pow_Cheak)
            {
                double New_dim = Math.Pow(2, Pow_Ind);

                for (int i = pictureBox1.Height; i < New_dim; i++)
                {
                    List<double> InCupol = new List<double>();
                    for (int j = 0; j < pictureBox1.Width; j++)
                    {
                        InCupol.Add(0);
                    }
                    Cupola.Add(InCupol);
                }
            }

            Spec_1D = new List<Complex>();
            Spec_2D = new List<List<Complex>>();
        }

        private void button3_Click(object sender, EventArgs e)
        {

        }

        private void button4_Click(object sender, EventArgs e)
        {

        }
        public double GausF(double x, double y, Points NullP, int sigma) // функция гауса
        {
            double fx, yy, xx;

            yy = Math.Exp(-((y - NullP.Y) * (y - NullP.Y)) / sigma);
            xx = Math.Exp(-((x - NullP.X) * (x - NullP.X)) / sigma);

            fx = yy * xx;
            return fx;
        }
        public void Chek_For2(int A, ref bool Has, ref int idd) // функция гауса
        {
            
            for (int i = 0; i < 11; i++) 
            {

                double d = Math.Pow(2, i);
                if (A < d) { idd = i; break; }

                if (A == d) { Has = false; break; }
            }
           // idd += 1;
        }
        // поиск максимума в 2д массиве
        public double FindMax(in List<List<double>> L) 
        {
            double Fm = 0;

            for (int i = 0; i < pictureBox1.Height; i++)
            {
                for (int j = 0; j < pictureBox1.Width; j++)
                {
                    if (L[i][j] > Fm) Fm = L[i][j];
                }
            }

            return Fm;
        }

        // заполняем двумерный массив
        public void Mass_Funk(ref List<List<double>> Cup) 
        {
            double fx;
            Points P1 = new Points();
            Points P2 = new Points();
            Points P3 = new Points();

            P1.X = 150;
            P1.Y = 150;

            P2.X = 300;
            P2.Y = 240;

            P3.X = 240;
            P3.Y = 310;

            for (int i = 0; i < pictureBox1.Height; i++)
            {
                List<double> InCupol = new List<double>();

                for (int j = 0; j < pictureBox1.Width; j++)
                {

                    fx = (GausF(i, j, P1, 7000) + GausF(i, j, P2, 12000) + GausF(i, j, P3, 17500));
                    InCupol.Add(fx);
                }

                Cup.Add(InCupol); //ЗАПОЛНЯЕМ ДВУМЕРНЫЙ МАСИВ

            }
        }
        public void ShumPod(ref List<List<double>> Cup) // НАЛОЖЕНИЕ ШУМА
        {
            
            Points Po = new Points(0, 0);
            int xx = pictureBox1.Width / 2;
            int yy = pictureBox1.Height / 2;
            double Sun = (double)(Shum_lvl) / 100;

            for (int i = 0; i < pictureBox1.Height; i++)
            {

                for (int j = 0; j < pictureBox1.Width; j++)
                {
                    Random rtx = new Random();
                    int dx = rtx.Next(-xx, xx);

                    Random rty = new Random();
                    int dy = rty.Next(-yy, yy);

                    Cup[i][j] = Cup[i][j] + Sun * GausF(dx, dy, Po, 10000);
                }


            }
            //int z = yy * xx;
        }
        //void fourea(Complex[] data, int n, int iss)
        //{
        //    int i, j, istep;
        //    int m, mmax;
        //    double r, r1, theta, w_r, w_i, temp_r, temp_i;
        //    double pi = 3.1415926f;

        //    r = pi * iss;
        //    j = 0;
        //    for (i = 0; i < n; i++)
        //    {
        //        if (i < j)
        //        {
        //            temp_r = data[j].Real;
        //            temp_i = data[j].Imaginary;
        //            data[j] = data[i];
        //            data[i] = new Complex(temp_r, temp_i);

        //        }
        //        m = n >> 1;
        //        while (j >= m) { j -= m; m = (m + 1) / 2; }
        //        j += m;
        //    }
        //    mmax = 1;
        //    while (mmax < n)
        //    {
        //        istep = mmax << 1;
        //        r1 = r / (float)mmax;
        //        for (m = 0; m < mmax; m++)
        //        {
        //            theta = r1 * m;
        //            w_r = (float)Math.Cos((double)theta);
        //            w_i = (float)Math.Sin((double)theta);
        //            for (i = m; i < n; i += istep)
        //            {
        //                j = i + mmax;
        //                temp_r = w_r * data[j].Real - w_i * data[j].Imaginary;
        //                temp_i = w_r * data[j].Imaginary + w_i * data[j].Real;
        //                Complex cmplx = new Complex(data[i].Real - temp_r, data[i].Imaginary - temp_i);
        //                data[j] = new Complex(data[i].Real - temp_r, data[i].Imaginary - temp_i);
        //                data[i] = new Complex(data[i].Real + temp_r, data[i].Imaginary + temp_i);
        //            }
        //        }
        //        mmax = istep;
        //    }
        //    if (iss > 0)
        //        for (i = 0; i < n; i++)
        //        {

        //            data[i] = new Complex(data[i].Real / (float)n, data[i].Imaginary / (float)n);
        //        }

        //}
        // Фурье
      
        public void Fur_Funk(ref List<Complex> data, int n, int iso)// Быстрое Фурье
        {
           //List<Complex> data = new
            int i, j, istep;
            int m, mmax;
            double r, r1, theta, w_r, w_i, temp_r, temp_i;

            r = Math.PI * iso;
            j = 0;
            for (i = 0; i < n; i++)
            {
                if (i < j)
                {
                    temp_r = data[j].Real;
                    temp_i = data[j].Imaginary;
                    data[j] = data[i];
                    data[i] = new Complex(temp_r, temp_i);

                }
                m = n >> 1;
                while (j >= m)
                {
                    j -= m; m = (m + 1) / 2;
                }
                j += m;
            }
            mmax = 1;
            while (mmax < n)
            {
                istep = mmax << 1;
                r1 = r / (float)mmax;
                for (m = 0; m < mmax; m++)
                {
                    theta = r1 * m;
                    w_r = Math.Cos((double)theta);
                    w_i = Math.Sin((double)theta);
                    for (i = m; i < n; i += istep)
                    {
                        j = i + mmax;
                        temp_r = w_r * data[j].Real - w_i * data[j].Imaginary;
                        temp_i = w_r * data[j].Imaginary + w_i * data[j].Real;

                        data[j] = new Complex(data[i].Real - temp_r, data[i].Imaginary - temp_i);
                        data[i] = new Complex(data[i].Real + temp_r, data[i].Imaginary + temp_i);
                    }
                }
                mmax = istep;
            }
            if (iso > 0)
                for (i = 0; i < n; i++)
                {
                    data[i] = new Complex(data[i].Real / (float)n, data[i].Imaginary / (float)n);
                }
        }

        // ВЫВОДИМ 2Д В ЧБ
        public void DrawB_to_w(in List<List<double>> Cupola5, Graphics graph5) 
        {
            double CupMax = FindMax(Cupola5);
            SolidBrush BrushBleak = new SolidBrush(Color.Black);


            for (int i = 0; i < pictureBox1.Height; i++) // РИСУЕМ ПО УРОВНЯМ
            {
                for (int j = 0; j < pictureBox1.Width; j++)
                {
                    if (Cupola5[i][j] > 0.2 * CupMax) graph5.FillEllipse(BrushGray, i, j, 5, 5);


                }

            }
            for (int i = 0; i < pictureBox1.Height; i++)
            {
                for (int j = 0; j < pictureBox1.Width; j++)
                {

                    if (Cupola5[i][j] > 0.4 * CupMax) graph5.FillEllipse(BrushDarkGray, i, j, 5, 5);


                }

            }
            for (int i = 0; i < pictureBox1.Height; i++)
            {
                for (int j = 0; j < pictureBox1.Width; j++)
                {

                    if (Cupola5[i][j] > 0.6 * CupMax) graph5.FillEllipse(BrushLightGray, i, j, 5, 5);


                }

            }

            for (int i = 0; i < pictureBox1.Height; i++)
            {
                for (int j = 0; j < pictureBox1.Width; j++)
                {
                    // Cupola[j][i]
                    if (Cupola5[i][j] > 0.8 * CupMax)
                    {
                        int yu = 0;
                        graph5.FillEllipse(BrushWhite, i, j, 5, 5);
                    }


                }

            }

            pictureBox1.Image = bmp;
        }
    }
}
//void CSignalDlg::Spectoor()
//{
//    UpdateData(TRUE);

//    cmplx = new Complex[arraysize];
//    for (int i = 0; i < arraysize; i++)
//    {
//        cmplx[i].real = Shum[i];
//        cmplx[i].image = 0;
//    }
//    fourea(cmplx, arraysize, -1);
//    Spectr = new double[arraysize];
//    for (int i = 0; i < arraysize; i++)
//    {
//        Spectr[i] = sqrt((cmplx[i].real * cmplx[i].real) + (cmplx[i].image * cmplx[i].image));
//    }
//}
//void CSignalDlg::RCVSignal()
//{
//    //Рабочий вариант
//    UpdateData(TRUE);
//    Spectoor();

//    EnergOfSpektr = 0;
//    for (int i = 0; i < arraysize / 2; i++)
//    {
//        EnergOfSpektr += Spectr[i];
//    }

//    EnergOfSpektr = EnergOfSpektr * recovery / 100;

//    int count1(0), count2(0);
//    double Ep(0);
//    for (int i = 0; i < arraysize / 2; i++)
//    {
//        Ep += Spectr[i];
//        count1 = i + 1;
//        if (Ep >= EnergOfSpektr) break;
//    }
//    count2 = arraysize - count1;

//    for (int i = count1; i < count2; i++)
//    {
//        cmplx[i].real = 0;
//        cmplx[i].image = 0;
//    }
//    vcmplx = new Complex[arraysize];
//    for (int i = 0; i < arraysize; i++)
//    {
//        vcmplx[i].real = cmplx[i].real;
//        vcmplx[i].image = cmplx[i].image;
//    }
//    VSpectr = new double[arraysize];
//    for (int i = 0; i < arraysize; i++)
//    {
//        VSpectr[i] = sqrt((vcmplx[i].real * vcmplx[i].real) + (vcmplx[i].image * vcmplx[i].image));

//    }
//    fourea(vcmplx, arraysize, 1);
//    RCVSIGNAL = new double[arraysize];
//    for (int i = 0; i < arraysize; i++)
//    {
//        RCVSIGNAL[i] = vcmplx[i].real;
//    }

//}
